@extends('layouts.admin')

@section('content')
    {{$subscribers->links()}}
    <table class="table">
        <caption>Пользователи</caption>
        <tr>
            <th>E-mail</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        @foreach($subscribers as $item)
        <tr>
            <td>{{$item->email}}</td>
            <td>{{$item->created_at->format('d.m.Y H:i')}}</td>
            <td>
                <form action="{{route('admin.subscribers.destroy', $item->id)}}" class="d-inline-block confirmed" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    {{$subscribers->links()}}
@stop