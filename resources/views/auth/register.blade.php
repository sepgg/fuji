@extends('layouts.site')

@section('content')
<div class="container">
<div class="row  justify-content-center">
    <div class="col-md-4">
        <form autocomplete="off" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <label>{{__('ui.auth.name')}}</label>
                <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name')}}">
                @error('name') <span class="invalid-feedback">{{$message}}</span> @enderror
            </div>
            <div class="form-group">
                <label>{{__('ui.auth.email')}}</label>
                <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" value="{{old('email')}}">
                @error('email') <span class="invalid-feedback">{{$message}}</span> @enderror
            </div>
            <div class="form-group">
                <label>{{__('ui.auth.password')}}</label>
                <input class="form-control @error('password') is-invalid @enderror" type="password" name="password">
                @error('password') <span class="invalid-feedback">{{$message}}</span> @enderror
            </div>
            <div class="form-group">
                <label>{{__('ui.auth.confirm')}}</label>
                <input class="form-control" type="password" name="password_confirmation">
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input @error('rules') is-invalid @enderror" type="checkbox" name="rules" checked id="accept_rules"> <span class="ml-2">{{__('ui.auth.accept')}} <a href="/files/offer.pdf" target="_blank">{{__('ui.auth.terms')}}</a></span>
                    @error('rules')<span class="invalid-feedback">{{$message}}</span> @enderror
                </div>
            </div>
            <button class="btn btn-block mb-3 btn-primary rounded-pill" id="reg_button">{{__('ui.auth.register')}}</button>
            <div>{{__('ui.auth.has')}} <strong><a href="{{route('login')}}">{{__('ui.auth.signin')}}</a></strong></div>
        </form>
    </div>
</div>
</div>
@endsection
