@extends('layouts.site')

@section('title')
{{$startup->name}}
@stop

@section('content')
<div class="container">
    <div class="card card-default card-body mb-3">
        <h1>{{$startup->name}}</h1>
        <hr>
        <div>
            @if($startup->image)
                <img src="/storage/{{$startup->image}}" alt="" class="img-fluid pull-left mb-3 mr-3 col-md-3 col px-0">
            @endif
            <div class="d-inline">
                <p class="lead">{{$startup->preview}}</p>
                <p class="text-muted">{!!$startup->text!!}</p>
            </div>
        </div>
    </div>
</div>
@stop