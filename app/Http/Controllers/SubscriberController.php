<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Subscriber;
class SubscriberController extends Controller
{
    public function index(){
        $subscribers = Subscriber::latest()->paginate();
        return view('admin.subscribers.index', compact('subscribers'));
    }
    public function store(Request $request){
        $validator = Validator($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'subscribe.validate');
        }elseif(Subscriber::whereEmail($request->email)->count()){
            session()->flash('error', 'subscribe.active');
        }else {
            Subscriber::create($request->all());
            session()->flash('success', 'subscribe.success');
        }
        return back();
    }
}
